<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Book;
use app\models\Subscription;
use app\models\SubscriptionHistory;
use yii\console\Controller;
use yii\helpers\Url;
use Yii;

class SubscriptionController extends Controller
{
    public function actionSend()
    {
        $count = 0;
        $start = date("Y-m-d 00:00");
        $end = date("Y-m-d 23:59");

        $count_books = Book::find()
            ->where([
                'between',
                'created_at',
                $start,
                $end
            ])->count();

        if ($count_books) {
            $subQuery = SubscriptionHistory::find()
                ->where('subscription.id = subscription_history.subscription_id')
                ->andWhere([
                    'between',
                    'subscription_history.created_at',
                    $start,
                    $end
                ]);

            $subsriptions = Subscription::find()
                ->where(['not exists', $subQuery])
                ->all();

            $transaction = Yii::$app->db->beginTransaction();
            try {
                foreach ($subsriptions as $subsription) {
                    /** @var  Subscription $subsription */
                    $phone = str_replace([')','(','-'], '', $subsription->phone);
                    $send = Yii::$app->sms->send(
                        $phone,
                        "У нас новые поступления книг"
                    );
                    $history = new SubscriptionHistory();
                    $history->subscription_id = $subsription->id;
                    $history->status = ($send) ? SubscriptionHistory::STATUS_DONE : SubscriptionHistory::STATUS_ERROR;

                    if (!$history->save()) {
                        throw new \Exception("Error add to record history" . json_encode($history->errors));
                    }
                    $count++;
                }

                $transaction->commit();

                return $count;
            } catch (\Exception $e) {
                $transaction->rollBack();
                return "Error: " . $e->getMessage();
            }
        } else {
            return false;
        }
    }
}
