<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\models\Author;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=Html::activeHiddenInput($model, 'user_id'); ?>

    <?= $form->field($model, 'titile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isbn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php if ($model->foto) : ?>
        <?=Html::img($model->foto, ['width'=>'100px'])?>
    <?php endif; ?>

    <?= $form->field($model, 'file')->fileInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'author_ids')
        ->dropDownList(
            Author::dataForSelect(),
            ['multiple' => 'multiple']
        );
    ?>


    <?= $form->field($model, 'year')
        ->widget(
            DatePicker::className(),
            [
                'dateFormat' => 'yyyy-MM-dd',
                'clientOptions' => [
                    'defaultDate' => date("Y-m-d"),

                ]
            ]
        );
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('book', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
