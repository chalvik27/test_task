<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubscriptionHistory */

$this->title = Yii::t('subscription', 'Create Subscription History');
$this->params['breadcrumbs'][] = ['label' => Yii::t('subscription', 'Subscription Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
