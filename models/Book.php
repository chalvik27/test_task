<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string $titile Название
 * @property string $description Описание
 * @property string $foto Фото
 * @property string $year Дата выпуска
 * @property int $user_id user created record
 * @property string $created_at
 * @property string $updated_at
 */
class Book extends ActiveRecord
{

    public $author_ids;
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['author_ids', 'each', 'rule' => ['integer']],
            [['titile', 'description', 'year', 'user_id', 'isbn'], 'required'],
            [['description', 'isbn'], 'string'],
            [['isbn'], 'validateIsBn'],
            [['year', 'created_at', 'updated_at'], 'safe'],
            [['user_id'], 'integer'],
            [['titile', 'foto'], 'string', 'max' => 255],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateIsBn($attribute, $params)
    {
        if (!preg_match("/^(\d{1,}-){0,1}\d{1,}-\d{1,}-\d{1,}-\d{1,}$/", $this->$attribute)) {
            $this->addError($attribute, 'Неверный формат prefix-code-code-code-code или code-code-code-code');
        }
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => gmdate("Y-m-d H:i:s"),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('book', 'ID'),
            'titile' => Yii::t('book', 'Titile'),
            'description' => Yii::t('book', 'Description'),
            'foto' => Yii::t('book', 'Foto'),
            'year' => Yii::t('book', 'Year'),
            'isbn' => Yii::t('book', 'Is Bn'),
            'user_id' => Yii::t('book', 'User ID'),
            'created_at' => Yii::t('book', 'Created At'),
            'updated_at' => Yii::t('book', 'Updated At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getAuthors()
    {
        return $this->hasMany(Author::className(), ['id' => 'author_id'])
            ->viaTable('book_author', ['book_id' => 'id']);
    }

    /**
     * After Find
     */
    public function afterFind()
    {
        parent::afterFind();
        foreach ($this->authors as $autor) {
            $this->author_ids[] = $autor->id;
        }
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->linkToAuthors();
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $result = parent::beforeSave($insert);
        if ($result) {
            $this->uploadFile();
        }
        return $result;
    }

    /**
     * Add Link or onLink to Author
     */
    private function linkToAuthors()
    {
        $author_ids = [];
        $this->author_ids = (is_array($this->author_ids))?$this->author_ids:[];

        $authors = $this->authors;

        foreach ($authors as $author) {
            $author_ids[] = $author->id;
            if (!in_array($author->id, $this->author_ids)) {
                $this->unlink('authors', $author, true);
            }
        }
        foreach ($this->author_ids as $id) {
            if (!in_array($id, $author_ids)) {
                $author = Author::find()->where(['id' => $id])->one();
                if ($author) {
                    $this->link('authors', $author);
                }
            }
        }
    }

    /**
     * Upload file for field foto
     * @return bool
     */
    private function uploadFile()
    {
        $file = UploadedFile::getInstance($this, 'file');
        if ($file) {
            $path = '/upload/'  . $file->baseName . '.' . $file->extension;
            if ($file->saveAs(Yii::getAlias('@app/web').$path)) {
                $this->foto = $path;
            }
        }
    }
}