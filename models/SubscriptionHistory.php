<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "subscription_history".
 *
 * @property int $id
 * @property int $subscription_id
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class SubscriptionHistory extends ActiveRecord
{

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => gmdate("Y-m-d H:i:s"),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    const STATUS_ERROR = -1;
    const STATUS_DONE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscription_id', 'status'], 'integer'],
            [['status'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('subscription', 'ID'),
            'subscription_id' => Yii::t('subscription', 'Subscription ID'),
            'status' => Yii::t('subscription', 'Status'),
            'created_at' => Yii::t('subscription', 'Created At'),
            'updated_at' => Yii::t('subscription', 'Updated At'),
        ];
    }

}
