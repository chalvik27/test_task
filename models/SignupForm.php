<?php

namespace app\models;

use app\models\User;
use Yii;

/**
 * Registration form collects user input on registration process, validates it and creates new User model.
 *
 * @author lexa
 */
class SignupForm extends User
{

    public function rules()
    {
        return [
            // username rules
            'usernameLength'   => ['username', 'string', 'min' => 3, 'max' => 255],
            'usernameTrim'     => ['username', 'filter', 'filter' => 'trim'],
            'usernameRequired' => ['username', 'required'],
            'usernameUnique'   => [
                'username',
                'unique',
                'targetClass' => $this,
                'message' => Yii::t('user', 'This username has already been taken')
            ],
            // email rules
            'emailTrim'     => ['email', 'filter', 'filter' => 'trim'],
            'emailRequired' => ['email', 'required'],
            'emailPattern'  => ['email', 'email'],
            'emailUnique'   => [
                'email',
                'unique',
                'targetClass' => $this,
                'message' => Yii::t('user', 'This email address has already been taken')
            ],
            // password rules
            'passwordRequired' => ['password', 'required'],
            'passwordLength'   => ['password', 'string', 'min' => 6, 'max' => 72],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'    => Yii::t('user', 'Email'),
            'username' => Yii::t('user', 'Username'),
            'password' => Yii::t('user', 'Password'),
        ];
    }


    /**
     * Registers a new user account. If registration was successful it will set flash message.
     *
     * @return bool
     */
    public function register()
    {

        if (!$this->validate()) {
            return false;
        }

        $this->generateAuthKey();
        $this->generatePasswordResetToken();
        $this->setPassword($this->password);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (!$this->save()) {
                $transaction->rollBack();
                return false;
            }

            $transaction->commit();

            Yii::$app->session->setFlash(
                'info',
                Yii::t(
                    'user',
                    'Your account has been created'
                )
            );
            return $this;
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash(
                'error',
                Yii::t(
                    'user',
                    $e->getMessage()
                )
            );
        }

        return false;
    }

}
