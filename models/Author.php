<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "author".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $patronymic
 * @property string $created_at
 * @property string $updated_at
 */
class Author extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'author';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'patronymic'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name', 'patronymic'], 'string', 'max' => 255],
        ];
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => gmdate("Y-m-d H:i:s"),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('author', 'ID'),
            'first_name' => Yii::t('author', 'First Name'),
            'last_name' => Yii::t('author', 'Last Name'),
            'patronymic' => Yii::t('author', 'Patronymic'),
            'created_at' => Yii::t('author', 'Created At'),
            'updated_at' => Yii::t('author', 'Updated At'),
        ];
    }

    /**
     * Get array authors for select
     * @return array
     */
    public static function dataForSelect()
    {
        $result = [];
        $authors = self::find()->all();
        foreach ($authors as $author) {
            /* @var $author Author */
            $result[$author->id] = $author->last_name.' '.$author->first_name.' '.$author->patronymic;
        }
        return $result;
    }

    /**
     * @return ActiveQuery
     */
    public function getBooks()
    {
        return $this->hasMany(Book::className(), ['id' => 'book_id'])
            ->viaTable('book_author', ['author_id' => 'id']);
    }
}
