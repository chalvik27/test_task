<?php
/**
 * Created by PhpStorm.
 * User: lexa
 * Date: 23.01.18
 * Time: 14:11
 */

namespace app\widgets;



use app\models\Author;
use app\models\Book;
use app\models\search\BookSearch;

/**
 * Class TopBook
 * @package app\widgets
 * @param integer $year
 * @param integer $limit
 */
class TopBook extends \yii\base\Widget
{
    public $limit = 10;
    public $year;
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $limit = (int)$this->limit;
        $year = (int)$this->year;
        $year = ($year > 2000)?(int)$this->year:date("Y");

        $start =  date("Y-m-d", strtotime("$year-1-1 00:00"));
        $end =  date("Y-m-d", strtotime("$year-12-31 00:00"));

        echo $start.'==='.$end;

        $authors = Author::find()
            ->select(['author.*','count' => 'COUNT(book.id)'])
            ->joinWith(['books'])
            ->where(['between','book.year',$start, $end])
            ->OrderBy('count DESC')
            ->groupBy('author.id')
            ->limit($limit)
        ->all();

        return $this->render('view_top', [
            'authors' => $authors
        ]);
    }
}