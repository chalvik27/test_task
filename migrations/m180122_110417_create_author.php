<?php

use yii\db\Migration;

/**
 * Class m180122_110417_create_author
 */
class m180122_110417_create_author extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%author}}',
            [
                'id'        => $this->primaryKey(),
                'first_name'    => $this->string()->notNull(),
                'last_name'     => $this->string()->notNull(),
                'patronymic'    => $this->string()->notNull(),
                'created_at'    => $this->timestamp()->defaultValue(null),
                'updated_at'    => $this->timestamp()->defaultValue(null),
            ],
            $tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%author}}');
        return true;
    }
}
