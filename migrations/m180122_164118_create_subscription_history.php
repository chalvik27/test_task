<?php

use yii\db\Migration;

/**
 * Class m180122_164118_create_subscription_history
 */
class m180122_164118_create_subscription_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%subscription_history}}',
            [
                'id'                => $this->primaryKey(),
                'subscription_id'   => $this->integer(),
                'status'            => $this->integer()->notNull(),
                'created_at'        => $this->timestamp()->defaultValue(null),
                'updated_at'        => $this->timestamp()->defaultValue(null),
            ],
            $tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%subscription_history}}');
        return true;
    }
}
