<?php

use yii\db\Migration;

/**
 * Class m180122_110435_create_book
 */
class m180122_110435_create_book extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%book}}',
            [
                'id'            => $this->primaryKey(),
                'titile'        => $this->string()->notNull()->comment("Название"),
                'description'   => $this->text()->notNull()->comment("Описание"),
                'foto'          => $this->string()->comment("Фото"),
                'year'          => $this->date()->notNull()->comment("Дата выпуска"),
                'user_id'       => $this->integer()->notNull()->comment("user created record"),
                'created_at'    => $this->timestamp()->defaultValue(null),
                'updated_at'    => $this->timestamp()->defaultValue(null),
            ],
            $tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%book}}');
        return true;
    }
}
