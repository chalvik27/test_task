<?php

use yii\db\Migration;

/**
 * Class m180122_170156_update_book_add_isbn
 */
class m180122_170156_update_book_add_isbn extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%book}}', 'isbn', $this->string()->after('id'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%book}}', 'isbn');
        return true;
    }

}
