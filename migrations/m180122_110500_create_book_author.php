<?php

use yii\db\Migration;

/**
 * Class m180122_110500_create_book_author
 */
class m180122_110500_create_book_author extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%book_author}}',
            [
                'book_id'       => $this->integer()->notNull(),
                'author_id'     => $this->integer()->notNull(),
            ],
            $tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%book_author}}');
        return true;
    }

}
