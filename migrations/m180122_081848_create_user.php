<?php

use yii\db\Migration;

/**
 * Class m180122_081848_create_user
 */
class m180122_081848_create_user extends Migration
{
    /**
     * Create table user
     *
     * @return mixed
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%user}}',
            [
                'id'        => $this->primaryKey(),
                'username'  => $this->string()->notNull()->unique(),
                'auth_key'  => $this->string(32)->notNull(),
                'password_hash' => $this->string()->notNull(),
                'password_reset_token' => $this->string()->unique(),
                'email'         => $this->string()->notNull()->unique(),
                'status'        => $this->smallInteger()->notNull()->defaultValue(1),
                'created_at'    => $this->timestamp()->defaultValue(null),
                'updated_at'    => $this->timestamp()->defaultValue(null),
            ],
            $tableOptions
        );
    }

    /**
     * Drop table
     *
     * @return boolean
     */
    public function down()
    {
        $this->dropTable('{{%user}}');
        return true;
    }
}
