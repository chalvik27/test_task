<?php

use yii\db\Migration;

/**
 * Class m180122_164111_create_subscription
 */
class m180122_164111_create_subscription extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%subscription}}',
            [
                'id'            => $this->primaryKey(),
                'user_id'       => $this->integer(),
                'phone'         => $this->string()->notNull(),
                'created_at'    => $this->timestamp()->defaultValue(null),
                'updated_at'    => $this->timestamp()->defaultValue(null),
            ],
            $tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%subscription}}');
        return true;
    }

}
