<?php

namespace app\components;



/**
 * Class SmsPilot
 *
 *  SMS Pilot API/PHP v1.8
 * SEE: http://www.smspilot.ru/apikey.php
 * @package app
 */

class SmsPilot extends \yii\base\Component
{

    private $api = 'http://smspilot.ru/api.php';
    public $apikey = 'XXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZXXXXXXXXXXXXYYYYYYYYYYYYZZZZZZZZ';
    public $charset = 'UTF-8';
    public $from = '';


    /**
     * @param $to
     * @param $text
     * @param bool $from
     * @param bool $send_datetime
     * @return array|bool
     */
    public function send($to, $text, $from = false, $send_datetime = false)
    {

        if ($this->charset != 'UTF-8') {
            $text = mb_convert_encoding($text, 'utf-8', $this->charset);
        }
        $result = $this->smsHttpPost($this->api, array(
            'send' => $text,
            'to' => (is_array($to) ? implode(',', $to) : $to),
            'from' => (($from) ? urlencode($from) : $this->from),
            'send_datetime' => $send_datetime,
            'apikey' => $this->apikey
        ));

        if ($result) {
            if (substr($result, 0, 6) == 'ERROR=') {
                $this->smsError(substr($result, 6));
                return false;
            } elseif (substr($result, 0, 8) == 'SUCCESS=') {
                $success = substr($result, 8, ($p = strpos($result, "\n")) - 8);
                $this->smsSuccess($success);
                // SENT 24.50/1360.90
                if (preg_match('~([0-9.]+)/([0-9.]+)~', $success, $matches)) {
                    $this->smsCost($matches[1]); // new in 1.8
                    $this->smsBalance($matches[2]); // new in 1.8
                }
                //status
                $status_csv = substr($result, $p + 1);
                $status_csv = explode("\n", $status_csv);
                $status = array();
                foreach ($status_csv as $line) {
                    $s = explode(',', $line);
                    $status[] = array(
                        'id' => $s[0],
                        'phone' => $s[1],
                        'price' => $s[2],
                        'status' => $s[3]
                    );
                }
                $this->smsStatus($status);

                return $status;
            } else {
                $this->smsError('UNKNOWN RESPONSE');
                return false;
            }
        } else {
            $this->smsError('CONNECTION ERROR');
            return false;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function smsCheck($id)
    {
        $result = $this->smsHttpPost($this->api, array(
            'check' => (is_array($id) ? implode(',', $id) : $id),
            'apikey' => $this->apikey
        ));

        if ($result) {
            if (substr($result, 0, 6) == 'ERROR=') {
                $this->smsError(substr($result, 6));
                return false;
            } else {
                $status_csv = $result;
                //status
                $status_csv = explode("\n", $status_csv);
                $status = array();
                foreach ($status_csv as $line) {
                    $s = explode(',', $line);
                    $status[] = array(
                        'id' => $s[0],
                        'phone' => $s[1],
                        'price' => $s[2],
                        'status' => $s[3]
                    );
                }
                return $this->smsStatus($status);
            }
        } else {
            $this->smsError('CONNECTION ERROR');
            return false;
        }
    }

    /**
     * @param null $set
     * @return bool|null
     */
    public function smsBalance($set = null)
    {
        static $b;
        if ($set !== null) {
            return $b = $set;
        } elseif (isset($b)) {
            return $b;
        }
        $result = $this->smsHttpPost(
            $this->api,
            [
                'balance' => 'rur',
                'apikey' => $this->apikey
            ]
        );

        if (strlen($result)) {
            if (substr($result, 0, 6) == 'ERROR=') {
                $this->smsError(substr($result, 6));
                return false;
            } else {
                return $b = $result;
            }
        } else {
            $this->smsError('CONNECTION ERROR');
            return false;
        }
    }

    /**
     * @return array|bool
     */
    public function smsInfo()
    {
        $result = $this->smsHttpPost($this->api, array(
            'apikey' => $this->apikey
        ));

        if ($result) {
            if (substr($result, 0, 6) == 'ERROR=') {
                $this->smsError(substr($result, 6));
            } elseif (substr($result, 0, 8) == 'SUCCESS=') {
                $s = substr($result, 8, $p = strpos($result, "\n"));
                $this->smsSuccess($s);
                $lines = explode("\n", substr($result, $p));
                $info = array();
                foreach ($lines as $line) {
                    if ($p = strpos($line, '=')) {
                        $info[substr($line, 0, $p)] = substr($line, $p + 1);
                    }
                }
                if ($this->charset != 'UTF-8') {
                    foreach ($info as $k => $v) {
                        $info[$k] = mb_convert_encoding($v, $this->charset, 'UTF-8');
                    }
                }
                return $info;
            }
        } else {
            $this->smsError('CONNECTION ERROR');
            return false;
        }
    }

    /**
     * @param null $set
     * @return bool|null
     */
    public function smsError($set = null)
    {
        static $e;
        if ($set !== null) {
            return $e = $set;
        } else {
            return (isset($e)) ? $e : false;
        }
    }

    /**
     * @param null $set
     * @return bool|null
     */
    public function smsSuccess($set = null)
    {
        static $s;
        if ($set !== null) {
            return $s = $set;
        } else {
            return (isset($s)) ? $s : false;
        }
    }

    /**
     * @param null $set
     * @return bool|null
     */
    public function smsStatus($set = null)
    {
        static $s;
        if ($set !== null) {
            return $s = $set;
        } else {
            return (isset($s)) ? $s : false;
        }
    }

    /**
     * @param null $set
     * @return int|null
     */
    public function smsCost($set = null)
    {
        static $c;
        if ($set !== null) {
            return $c = $set;
        } else {
            return (isset($c)) ? $c : 1;
        }
    }

    /**
     * sockets version HTTP/POST
     * @param $url
     * @param $data
     * @return bool|string
     */
    public function smsHttpPost($url, $data)
    {
        $eol = "\r\n";
        $post = '';
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $post .= $k . '=' . urlencode($v) . '&';
            }
            $post = substr($post, 0, -1);
            $content_type = 'application/x-www-form-urlencoded';
        } else {
            $post = $data;
            if (strpos($post, '<?xml') === 0) {
                $content_type = 'text/xml';
            } elseif (strpos($post, '{') === 0) {
                $content_type = 'application/json';
            } else {
                $content_type = 'text/html';
            }
        }

        if ((($u = parse_url($url)) === false) || !isset($u['host'])) {
            return false;
        }

        if (!isset($u['scheme'])) {
            $u['scheme'] = 'http';
        }

        $request = 'POST ' . (isset($u['path']) ? $u['path'] : '/') . ((isset($u['query'])) ? '?' . $u['query'] : '') . ' HTTP/1.1' . $eol
            . 'Host: ' . $u['host'] . $eol
            . 'Content-Type: ' . $content_type . $eol
            . 'Content-Length: ' . mb_strlen($post, 'latin1') . $eol
            . 'Connection: close' . $eol . $eol
            . $post;

        $host = ($u['scheme'] == 'https') ? 'ssl://' . $u['host'] : $u['host'];

        if (isset($u['port'])) {
            $port = $u['port'];
        } else {
            $port = ($u['scheme'] == 'https') ? 443 : 80;
        }

        $fp = @fsockopen($host, $port, $errno, $errstr, 10);
        if ($fp) {
            $content = '';
            $content_length = false;
            $chunked = false;
            fwrite($fp, $request);
            // read headers
            while ($line = fgets($fp)) {
                if (preg_match('/^HTTP\/[^\s]*\s(.*?)\s/', $line, $m) && $m[1] != 200) {
                    fclose($fp);
                    return false;
                } elseif (preg_match('~Content-Length: (\d+)~i', $line, $matches)) {
                    $content_length = (int)$matches[1];
                } elseif (preg_match('~Transfer-Encoding: chunked~i', $line)) {
                    $chunked = true;
                } elseif ($line == "\r\n") {
                    break;
                }
            }
            // read content
            if ($content_length !== false) {
                $_size = 4096;
                do {
                    $_data = fread($fp, $_size);
                    $content .= $_data;
                    $_size = min($content_length - strlen($content), 4096);
                } while ($_size > 0);
            } elseif ($chunked) {
                while ($chunk_length = hexdec(trim(fgets($fp)))) {
                    $chunk = '';
                    $read_length = 0;

                    while ($read_length < $chunk_length) {
                        $chunk .= fread($fp, $chunk_length - $read_length);
                        $read_length = strlen($chunk);
                    }
                    $content .= $chunk;

                    fgets($fp);
                }
            } else {
                while (!feof($fp)) {
                    $content .= fread($fp, 4096);
                }
            }
            fclose($fp);

            return $content;
        } else {
            return false;
        }
    }

}
